<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/mystyle.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content 
    {
    	height: 720px;
    } 
   
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #CF000F;
      color: white;
      padding: 5px;
    }

    header{
      background-color: #CF000F;
      color: white;
      padding: 5px;	
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<div class="container-fluid">

<div class="row">
	<header class="container-fluid text-center">
		<h3 class="Roboto-Regular">C&nbsp;A&nbsp;S&nbsp;H&nbsp;L&nbsp;E&nbsp;S&nbsp;S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&nbsp;I&nbsp;O&nbsp;S&nbsp;K</h4>
	</header>
</div>
  
  <div class="row content">  
    <div class="col-sm-9">
    	 @yield('content')
    </div>

<div class="col-sm-3 sidenav">
	<br><br><br>
	<div class="row">
		<div class="text-center">
      		<h3 class="Roboto-Regular">A&nbsp;T&nbsp;E&nbsp;N&nbsp;T&nbsp;I&nbsp;O&nbsp;N</h4>
    	</div>
    </div>
    <br>
    <div class="row">
    	<div class="col-md-12">
    		<div class="col-md-1">
    		<h5 class="Roboto-Regular">*</h5>
    		</div>
    		<div class="col-md-10 text-center">
    			<h5 class="Roboto-Regular">Silahkan pilih salah satu tenant favorite anda </h5>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="col-md-1">
    		<h5 class="Roboto-Regular">*</h5>
    		</div>
    		<div class="col-md-10 text-center">
    			<h5 class="Roboto-Regular">Pilih Product/Servis yang Anda Inginkan</h5>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="col-md-1">
    		<h5 class="Roboto-Regular">*</h5>
    		</div>
    		<div class="col-md-10 text-center">
    			<h5 class="Roboto-Regular">bayar Sesuai Harga yang ditampilkan</h5>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="col-md-1">
    		<h5 class="Roboto-Regular">*</h5>
    		</div>
    		<div class="col-md-10 text-center">
    			<h5 class="Roboto-Regular">Cetak Bukti pemesanan dan Bawa Ke Tenant Pilihan Anda</h5>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="col-md-1">
    		<h5 class="Roboto-Regular">*</h5>
    		</div>
    		<div class="col-md-10 text-center">
    			<h5 class="Roboto-Regular">Hubungi Customer Service atau petugas kami di 021 1234 123 Jika Anda Mengalami Kesulitan</h5>
    		</div>
    	</div>
    </div> 

  </div>
</div>
<div class="row">
<footer class="container-fluid">
	<div class="text-center">
  	<h3 class="Roboto-Regular">C&nbsp;A&nbsp;S&nbsp;H&nbsp;L&nbsp;E&nbsp;S&nbsp;S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&nbsp;I&nbsp;O&nbsp;S&nbsp;K</h4>
  	</div>
</footer>
</div>
</div>

</body>
</html>
